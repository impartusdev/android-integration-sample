package com.sample.app;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import com.impartus.lecturecapturelib.activities.VCNativeActivity;
import com.impartus.lecturecapturelib.app.AppController;
import com.impartus.lecturecapturelib.activities.VideoPlayerActivity;

public class MainActivity extends AppCompatActivity {

    final String TOKEN="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NDM2ODUsImV1bGEiOjEsImluc3RpdHV0ZUlkIjo1LCJ1c2VyVHlwZSI6MiwiaWF0IjoxNjU4MzA0NTIzLCJleHAiOjE2NTg5MDkzMjN9.Ffh2xOZNe_JdMPOcgW8OPj2QurQ3VtO1jhviZLB6GKI";
    final String TAG="MainActivity";

    ActivityResultLauncher<Intent> vcActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        if(data != null) {
                            VCNativeActivity.ExitCodes code = (VCNativeActivity.ExitCodes) data.getSerializableExtra("code");
                            switch (code){
                                case  USER_EXIT:
                                    Log.d(TAG,"VC Exit: User exited normally");
                                    break;
                                case LECTURE_NOT_STARTED:
                                    Log.d(TAG,"VC Exit: Lecture not started");
                                    break;
                                case INTERNET_ERROR:
                                    Log.d(TAG,"VC Exit: Connectivity Error");
                                    break;
                                case LECTURE_OVER:
                                    Log.d(TAG,"VC Exit: Lecture Over");
                                    break;
                                default:
                                    Log.d(TAG,"VC Exit: VC Activity Exit");
                            }
                        }
                    }
                }
            });;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppController.initLibrary(getApplication());

        Button vidPlayButton = findViewById(R.id.playvideo);
        Button launchVCButton = findViewById(R.id.launchvc);

        vidPlayButton.setOnClickListener(v->{
            playVideo();
        });

        launchVCButton.setOnClickListener(v->{
            checkVCPermissions();
        });
    }

    protected void playVideo(){
            Intent i = new Intent(getApplicationContext(), VideoPlayerActivity.class);
            i.putExtra(VideoPlayerActivity.KEY_TTID, Integer.parseInt("5758172"));
            i.putExtra(VideoPlayerActivity.KEY_TOKEN,TOKEN);
            i.putExtra(VideoPlayerActivity.KEY_INSTANCE,"https://a.impartus.com");
            startActivity(i);
    }

    private void checkVCPermissions(){
        if ((
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
        ) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 100);
        } else {
            launchVCActivity();
        }
    }
    private void launchVCActivity(){
        EditText ttidInput = findViewById(R.id.ttid);
        Intent i = new Intent(getApplicationContext(), VCNativeActivity.class);
        i.putExtra(VCNativeActivity.KEY_TTID, Integer.parseInt(ttidInput.getText().toString()));
        i.putExtra(VCNativeActivity.KEY_TOKEN,TOKEN);
        i.putExtra(VCNativeActivity.KEY_BASEURL,"https://a.impartus.com");
        vcActivityLauncher.launch(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100: {
                launchVCActivity();
            }
        }
    }
}
