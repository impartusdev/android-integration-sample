# Impartus Video Conferencing 
## Android Library - Integration notes



## Enable Java-8 and declare dependencies

### Add Repositories in build.gradle (Top level)
```
repositories {
  google()
  jcenter()
  mavenCentral()
  maven { url "https://jitpack.io" }
}
```

### Add the following lines in build.gradle (module)

```
android {
  ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
  packagingOptions {
    pickFirst "**/lib/**"
  }
}
dependencies {
  ...
  implementation ('com.impartus:app:2.0.10')
}
```

### Add the following lines in proguard-rules.pro (app)
```
-keep class org.webrtc.* { *; }
-keep class org.webrtc.voiceengine.* {*;}
-keep class org.webrtc.audio.* {*;}
```
## Add the following in the manifest

### Permissions:

```
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-feature android:name="android.hardware.camera" android:required="true" />
```

### Activity, Service and FileProvider

```
<activity
            android:name="com.impartus.lecturecapturelib.activities.VideoPlayerActivity"
            android:screenOrientation="portrait"
            android:configChanges="orientation|screenSize"
            android:label="@string/app_name"
            android:exported="true"
            android:theme="@style/AppTheme.NoActionBar"
            >
            <intent-filter>
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>
        <activity
            android:name="com.impartus.lecturecapturelib.activities.VCNativeActivity"
            android:supportsPictureInPicture="true"
            android:excludeFromRecents="true"
            android:autoRemoveFromRecents="true"
            android:configChanges="screenSize|smallestScreenSize|screenLayout|orientation"
            android:launchMode="singleTask"
            android:theme="@style/AppTheme.NoActionBar"
            android:exported="true"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.PHONE_STATE"/>
            </intent-filter>
        </activity>
        <service
            android:name="com.impartus.lecturecapturelib.vcnative.FGService"
            android:enabled="true"
            android:exported="true"
            android:foregroundServiceType="mediaProjection"
            />
<provider
    android:name="androidx.core.content.FileProvider"
    android:authorities="com.impartus.android.fileprovider"
    android:exported="false"
    android:grantUriPermissions="true">
    <meta-data
        android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/file_provider_paths"/>
</provider>
```


### Library initialization

```
import com.impartus.lecturecapturelib.app.AppController;
...
AppController.initLibrary(getApplication());
```

### Starting Video player activity

```
import com.impartus.lecturecapturelib.activities.VideoPlayerActivity;

...

Intent i = new Intent(getApplicationContext(), VideoPlayerActivity.class);

i.putExtra(VideoPlayerActivity.KEY_TTID, 5758172);
i.putExtra(VideoPlayerActivity.KEY_TOKEN,"token");
i.putExtra(VideoPlayerActivity.KEY_INSTANCE,"https://a.impartus.com");
startActivity(i);
```

- In the above code block, please replace the schedule id and token with the actual values. 

### Starting VC activity:

```
import com.impartus.lecturecapturelib.activities.VCNativeActivity;
...

ActivityResultLauncher<Intent> vcActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        if(data != null) {
                            VCNativeActivity.ExitCodes code = (VCNativeActivity.ExitCodes) data.getSerializableExtra("code");
                            switch (code){
                                case  USER_EXIT:
                                    Log.d(TAG,"VC Exit: User exited normally");
                                    break;
                                case LECTURE_NOT_STARTED:
                                    Log.d(TAG,"VC Exit: Lecture not started");
                                    break;
                                case INTERNET_ERROR:
                                    Log.d(TAG,"VC Exit: Connectivity Error");
                                    break;
                                case LECTURE_OVER:
                                    Log.d(TAG,"VC Exit: Lecture Over");
                                    break;
                                default:
                                    Log.d(TAG,"VC Exit: VC Activity Exit");
                            }
                        }
                    }
                }
            });;

...

Intent i = new Intent(getApplicationContext(), VCNativeActivity.class);

i.putExtra(VCActivity.KEY_TTID, 5046800);
i.putExtra(VCActivity.KEY_TOKEN,"token");
i.putExtra(VCActivity.KEY_BASEURL,"https://a.impartus.com");
vcActivityLauncher.launch(i);
```

- In the above code block, please replace the schedule id and token with the actual values. 
- Before starting the activity, please ask for the following runtime permissions:
  - Manifest.permission.CAMERA
  - Manifest.permission.RECORD_AUDIO
  - Manifest.permission.WRITE_EXTERNAL_STORAGE
  - Manifest.permission.READ_PHONE_STATE
